﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Gsb
{
    partial class Gsb : ServiceBase
    {
        public Gsb()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // Update the service state to Start Pending.
      

            // Set up a timer that triggers every minute.
            Timer timer = new Timer();
            timer.Interval = 30000; // 30 seconds
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.AutoReset = true;
            timer.Start();

            
        }

        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            MySqlCommand oCom = new MySqlCommand();
            ConnexionSql connexion;
            GestionDate laDate = new GestionDate();

            connexion = ConnexionSql.getInstance("127.0.0.1", "gsbv2", "root", "");
            connexion.openConnection();



            String jour = DateTime.Now.ToString("dd");
            String precedent = laDate.MoisPrecedent();

            int d = Convert.ToInt32(jour);
            if (d <= 10)
            {
                string req = "Update fichefrais set idEtat = 'CL'  where mois = " + precedent;



                oCom = connexion.reqExec(req);

                oCom.ExecuteNonQuery();
            }
            else
            {
                  if (d >= 20)
                {
                    string req = "Update fichefrais set idEtat = 'RB'  where mois = " + precedent;



                    oCom = connexion.reqExec(req);

                    oCom.ExecuteNonQuery();
                }
            }
            connexion.closeConnection();

        }

       


        protected override void OnStop()
        {
        }
    }
}
